
/**
 * 
 * @param {string} id 
 */
function converterUppercase(id) {
	var text = document.getElementById(id).value;
	document.getElementById(id).value = text.toUpperCase();
}

/**
 * 
 * @param {string} id 
 */
function configurandoDataTable(id) {
	$(document).ready(function () {
		$(id).DataTable({
			"language": {
				"emptyTable": "No hay datos disponibles en la tabla",
				"info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
				"infoEmpty": "No hay registros disponibles.",
				"infoFiltered": "(filtrado de _MAX_ registros totales)",
				"infoPostFix": "",
				"thousands": ",",
				"lengthMenu": "Mostrar _MENU_ registros",
				"loadingRecords": "Cargando...",
				"processing": "Procesando...",
				"search": "Buscar:",
				"zeroRecords": "No se encontraron registros coincidentes",
				"paginate": {
					"first": "Primero",
					"last": "Último",
					"next": "Siguiente",
					"previous": "Anterior"
				}
			}
		});
	});
}

/**
 * 
 * @param {*} id 
 */
function asignarFocus(id) {
	document.getElementById(id).focus();
}