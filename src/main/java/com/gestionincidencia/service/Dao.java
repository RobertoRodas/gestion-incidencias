package com.gestionincidencia.service;

import java.util.List;

public interface Dao<T> {

	public abstract T create(T entity);

	public abstract List<T> selectAll();

	public abstract T selectOne(Long id);

	public abstract void delete(Long id);

	
}
