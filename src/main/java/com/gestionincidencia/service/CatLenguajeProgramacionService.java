package com.gestionincidencia.service;

import java.util.List;

import com.gestionincidencia.entity.CatLenguajeProgramacion;

public interface CatLenguajeProgramacionService {

	public abstract CatLenguajeProgramacion create(CatLenguajeProgramacion catLenguajeProgramacion);

	public abstract List<CatLenguajeProgramacion> selectAll();

	public abstract CatLenguajeProgramacion selectOne(Long id);

	public abstract void delete(Long id);

}
