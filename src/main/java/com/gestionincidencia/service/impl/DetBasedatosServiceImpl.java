package com.gestionincidencia.service.impl;

import java.util.List;

import com.gestionincidencia.entity.DetBasedatos;
import com.gestionincidencia.repository.DetBasedatosRepository;
import com.gestionincidencia.service.Dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("detBasedatosServiceImpl")
public class DetBasedatosServiceImpl implements Dao<DetBasedatos> {

    @Autowired
    @Qualifier("detBasedatosRepository") 
    private DetBasedatosRepository repository;

    @Override
    public DetBasedatos create(DetBasedatos entity) {
        return repository.save(entity);
    }

    @Override
    public List<DetBasedatos> selectAll() {
        return repository.findAll();
    }

    @Override
    public DetBasedatos selectOne(Long id) {
        return repository.findById(id).get();
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

}