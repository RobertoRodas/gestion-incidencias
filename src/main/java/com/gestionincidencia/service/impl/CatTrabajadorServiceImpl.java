package com.gestionincidencia.service.impl;

import java.util.List;

import com.gestionincidencia.entity.CatTrabajador;
import com.gestionincidencia.repository.CatTrabajadorRepository;
import com.gestionincidencia.service.Dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("catTrabajadorServiceImpl")
public class CatTrabajadorServiceImpl implements Dao<CatTrabajador> {

    @Autowired
    @Qualifier("catTrabajadorRepository")
    private CatTrabajadorRepository repository;

    @Override
    public CatTrabajador create(CatTrabajador entity) {
        return repository.save(entity);
    }

    @Override
    public List<CatTrabajador> selectAll() {
        return repository.findAll();
    }

    @Override
    public CatTrabajador selectOne(Long id) {
        return repository.findById(id).get();
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

}