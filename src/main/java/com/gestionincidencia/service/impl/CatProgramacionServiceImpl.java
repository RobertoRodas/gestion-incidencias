package com.gestionincidencia.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.gestionincidencia.entity.CatProgramacion;
import com.gestionincidencia.repository.CatProgramacionRepository;
import com.gestionincidencia.service.Dao;

@Service("catProgramacionServiceImpl")
public class CatProgramacionServiceImpl implements Dao<CatProgramacion> {

	@Autowired
	@Qualifier("catProgramacionRepository")
	private CatProgramacionRepository repository;
	
	
	@Override
	public CatProgramacion create(CatProgramacion entity) {
		return repository.save(entity);
	}

	@Override
	public List<CatProgramacion> selectAll() {
		return repository.findAll();
	}

	@Override
	public CatProgramacion selectOne(Long id) {
		return repository.findById(id).get();
	}

	@Override
	public void delete(Long id) {
		repository.deleteById(id);
	}

}
