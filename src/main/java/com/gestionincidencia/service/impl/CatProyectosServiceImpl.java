package com.gestionincidencia.service.impl;

import java.util.List;

import com.gestionincidencia.entity.CatProyectos;
import com.gestionincidencia.repository.CatProyectosRepository;
import com.gestionincidencia.service.Dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("catProyectosServiceImpl")
public class CatProyectosServiceImpl implements Dao<CatProyectos> {

    @Autowired
    @Qualifier("catProyectosRepository")
    private CatProyectosRepository repository;

    
    /** 
     * @param entity
     * @return CatProyectos
     */
    @Override
    public CatProyectos create(CatProyectos entity) {
        return repository.save(entity);
    }

    
    /** 
     * @return List<CatProyectos>
     */
    @Override
    public List<CatProyectos> selectAll() {
        return repository.findAll();
    }

    
    /** 
     * @param id
     * @return CatProyectos
     */
    @Override
    public CatProyectos selectOne(Long id) {
        return repository.findById(id).get();
    }

    
    /** 
     * @param id
     */
    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

}