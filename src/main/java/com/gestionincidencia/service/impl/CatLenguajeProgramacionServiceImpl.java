package com.gestionincidencia.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.gestionincidencia.entity.CatLenguajeProgramacion;
import com.gestionincidencia.repository.CatLenguajeProgramacionRepository;
import com.gestionincidencia.service.CatLenguajeProgramacionService;

@Service("catLenguajeProgramacionServiceImpl")
public class CatLenguajeProgramacionServiceImpl implements CatLenguajeProgramacionService {

	@Autowired
	@Qualifier("catLenguajeProgramacionRepository")
	private CatLenguajeProgramacionRepository repository;
	
	@Override
	public CatLenguajeProgramacion create(CatLenguajeProgramacion catLenguajeProgramacion) {
		return repository.save(catLenguajeProgramacion);
	}

	@Override
	public List<CatLenguajeProgramacion> selectAll() {
		return repository.findAll();
	}

	@Override
	public CatLenguajeProgramacion selectOne(Long id) {
		return repository.findById(id).get();
	}

	@Override
	public void delete(Long id) {
		repository.deleteById(id);
	}

}
