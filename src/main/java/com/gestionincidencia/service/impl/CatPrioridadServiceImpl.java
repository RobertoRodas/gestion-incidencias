package com.gestionincidencia.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.gestionincidencia.entity.CatPrioridad;
import com.gestionincidencia.repository.CatPrioridadRepository;
import com.gestionincidencia.service.Dao;

@Service("catPrioridadServiceImpl")
public class CatPrioridadServiceImpl implements Dao<CatPrioridad> {

	@Autowired
	@Qualifier("catPrioridadRepository")
	private CatPrioridadRepository repository;
	
	@Override
	public CatPrioridad create(CatPrioridad entity) {
		return repository.save(entity);
	}

	@Override
	public List<CatPrioridad> selectAll() {
		return repository.findAll();
	}

	@Override
	public CatPrioridad selectOne(Long id) {
		return repository.findById(id).get();
	}

	@Override
	public void delete(Long id) {
		repository.deleteById(id);
	}

}
