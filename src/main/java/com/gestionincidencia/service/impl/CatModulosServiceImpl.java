package com.gestionincidencia.service.impl;

import java.util.List;

import com.gestionincidencia.entity.CatModulos;
import com.gestionincidencia.repository.CatModuloRepository;
import com.gestionincidencia.service.Dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("catModulosServiceImpl")
public class CatModulosServiceImpl implements Dao<CatModulos> {

    @Autowired
    @Qualifier("catModuloRepository")
    private CatModuloRepository repository;

    @Override
    public CatModulos create(CatModulos entity) {
        return repository.save(entity);
    }

    @Override
    public List<CatModulos> selectAll() {
        return repository.findAll();
    }

    @Override
    public CatModulos selectOne(Long id) {
        return repository.findById(id).get();
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

}