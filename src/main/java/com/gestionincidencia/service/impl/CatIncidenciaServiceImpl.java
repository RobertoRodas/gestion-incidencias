package com.gestionincidencia.service.impl;

import java.util.List;

import com.gestionincidencia.entity.CatIncidencia;
import com.gestionincidencia.repository.CatIncidenciaRepository;
import com.gestionincidencia.service.Dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("catIncidenciasServiceImpl")
public class CatIncidenciaServiceImpl implements Dao<CatIncidencia> {

    @Autowired
    @Qualifier("catIncidenciaRepository")
    private CatIncidenciaRepository repository;

    @Override
    public CatIncidencia create(CatIncidencia entity) {
        return repository.save(entity);
    }

    @Override
    public List<CatIncidencia> selectAll() {
        return repository.findAll();
    }

    @Override
    public CatIncidencia selectOne(Long id) {
        return repository.findById(id).get();
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

}