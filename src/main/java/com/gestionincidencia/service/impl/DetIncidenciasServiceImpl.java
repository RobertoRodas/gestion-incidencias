package com.gestionincidencia.service.impl;

import java.util.List;

import com.gestionincidencia.entity.DetIncidencia;
import com.gestionincidencia.repository.DetIncidenciasRepository;
import com.gestionincidencia.service.Dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("detIncidenciasServiceImpl")
public class DetIncidenciasServiceImpl implements Dao<DetIncidencia> {

    @Autowired
    @Qualifier("detIncidenciasRepository")
    private DetIncidenciasRepository repository;

    @Override
    public DetIncidencia create(DetIncidencia entity) {
        return repository.save(entity);
    }

    @Override
    public List<DetIncidencia> selectAll() {
        return repository.findAll();
    }

    @Override
    public DetIncidencia selectOne(Long id) {
        return repository.findById(id).get();
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

}