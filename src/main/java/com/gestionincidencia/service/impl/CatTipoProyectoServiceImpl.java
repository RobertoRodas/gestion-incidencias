package com.gestionincidencia.service.impl;

import java.util.List;

import com.gestionincidencia.entity.CatTipoProyecto;
import com.gestionincidencia.repository.CatTipoProyectoRepository;
import com.gestionincidencia.service.Dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("catTipoProyectoServiceImpl")
public class CatTipoProyectoServiceImpl implements Dao<CatTipoProyecto> {

    @Autowired
    @Qualifier("catTipoProyectoRepository")
    private CatTipoProyectoRepository repository;

    @Override
    public CatTipoProyecto create(CatTipoProyecto entity) {
        return repository.save(entity);
    }

    @Override
    public List<CatTipoProyecto> selectAll() {
        return repository.findAll();
    }

    @Override
    public CatTipoProyecto selectOne(Long id) {
        return repository.findById(id).get();
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);

    }

}