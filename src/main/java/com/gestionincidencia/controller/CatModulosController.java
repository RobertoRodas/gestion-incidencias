package com.gestionincidencia.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gestionincidencia.entity.CatModulos;
import com.gestionincidencia.service.impl.CatModulosServiceImpl;

@Controller
@RequestMapping("/catalogo/modulos")
public class CatModulosController {

	@Autowired
	@Qualifier("catModulosServiceImpl")
	private CatModulosServiceImpl serviceImpl;

	private final Log log = LogFactory.getLog(CatModulosController.class);

	@GetMapping("/")
	public String redireccionar() {
		return "redirect:/catalogo/modulos/listar";
	}

	@GetMapping("/listar")
	public String formulario(Model model) {
		model.addAttribute("listCLP", serviceImpl.selectAll());
		return Vistas.LISTAR_CAT_MODULOS;
	}

	@GetMapping("/addFormulario")
	public String agregarFormulario(@RequestParam(name = "id", required = true) int id, Model model) {
		CatModulos programacion = new CatModulos(0L);
		if (id != 0) {
			programacion = serviceImpl.selectOne((long) id);
		}
		log.info("agregarFormulario: " + programacion.toString());
		model.addAttribute("objectCLP", programacion);
		return Vistas.FORMULARIO_CAT_MODULOS;
	}

	@PostMapping("/addOrEdit")
	public String agregar(@ModelAttribute(name = "objectCLP") CatModulos catModulos, Model model) {

		log.info("object: " + catModulos.toString());
		serviceImpl.create(catModulos);
		return formulario(model);
	}

	@GetMapping("/remove")
	public String eliminar(@RequestParam(name = "id", required = true) int id, Model model) {
		serviceImpl.delete((long) id);
		return formulario(model);
	}
}
