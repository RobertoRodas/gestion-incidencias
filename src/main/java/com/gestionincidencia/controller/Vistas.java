package com.gestionincidencia.controller;

public class Vistas {

	public static final String FORMULARIO_CAT_LENGUAJE_PROGRAMACION = "catalogos/lenguaje_programacion/formulario";
	public static final String LISTAR_CAT_LENGUAJE_PROGRAMACION = "catalogos/lenguaje_programacion/listar";

	public static final String FORMULARIO_CAT_PRIORIDAD = "catalogos/prioridad/formulario";
	public static final String LISTAR_CAT_PRIORIDAD = "catalogos/prioridad/listar";

	public static final String FORMULARIO_CAT_PROGRAMACION = "catalogos/programacion/formulario";
	public static final String LISTAR_CAT_PROGRAMACION = "catalogos/programacion/listar";

	public static final String FORMULARIO_CAT_TRABAJADOR = "catalogos/trabajador/formulario";
	public static final String LISTAR_CAT_TRABAJADOR = "catalogos/trabajador/listar";

	public static final String FORMULARIO_CAT_MODULOS = "catalogos/modulos/formulario";
	public static final String LISTAR_CAT_MODULOS = "catalogos/modulos/listar";

	public static final String FORMULARIO_CAT_TIPO_PROYECTOS = "catalogos/tipo_proyecto/formulario";
	public static final String LISTAR_CAT_TIPO_PROYECTOS = "catalogos/tipo_proyecto/listar";

	public static final String FORMULARIO_CAT_PROYECTOS = "catalogos/proyectos/formulario";
	public static final String LISTAR_CAT_PROYECTOS = "catalogos/proyectos/listar";

	public static final String FORMULARIO_CAT_INCIDENCIA = "catalogos/incidencia/formulario";
	public static final String LISTAR_CAT_INCIDENCIA = "catalogos/incidencia/listar";

}
