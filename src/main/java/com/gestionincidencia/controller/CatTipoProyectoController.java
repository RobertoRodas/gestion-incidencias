package com.gestionincidencia.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gestionincidencia.entity.CatTipoProyecto;
import com.gestionincidencia.service.impl.CatTipoProyectoServiceImpl;


@Controller
@RequestMapping("/catalogo/tipo_proyecto")
public class CatTipoProyectoController {
	
	@Autowired
	@Qualifier("catTipoProyectoServiceImpl")
	private CatTipoProyectoServiceImpl serviceImpl;

	private final Log log = LogFactory.getLog(CatTipoProyectoController.class);

	@GetMapping("/")
	public String redireccionar() {
		return "redirect:/catalogo/tipo_proyecto/listar";
	}
	
	@GetMapping("/listar")
	public String formulario(Model model) {
		model.addAttribute("listCLP", serviceImpl.selectAll());
		return Vistas.LISTAR_CAT_TIPO_PROYECTOS;
	}
	
	
	@GetMapping("/addFormulario")
	public String agregarFormulario(@RequestParam(name = "id", required = true) int id,
	Model model) {
		CatTipoProyecto programacion = new CatTipoProyecto(0L);
		if (id != 0) {
			programacion = serviceImpl.selectOne((long) id);
		}
		log.info("agregarFormulario: " + programacion.toString());
		model.addAttribute("objectCLP", programacion);
		return Vistas.FORMULARIO_CAT_TIPO_PROYECTOS;
	}
	
	
	@PostMapping("/addOrEdit")
	public String agregar(@ModelAttribute(name="objectCLP") CatTipoProyecto catTipoProyecto, Model model) {
		if (catTipoProyecto.getIdCatTipoProyecto() == 0) {
			catTipoProyecto.setEstatus(true);			
		}
		
		log.info("object: " + catTipoProyecto.toString());
		serviceImpl.create(catTipoProyecto);
		return formulario(model);
	}
	
	
	@GetMapping("/remove")
	public String eliminar(@RequestParam(name = "id", required = true) int id, Model model){
		serviceImpl.delete((long) id);
		return formulario(model);
	}
}
