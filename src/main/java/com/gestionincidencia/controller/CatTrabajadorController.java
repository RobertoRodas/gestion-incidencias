package com.gestionincidencia.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gestionincidencia.entity.CatLenguajeProgramacion;
import com.gestionincidencia.entity.CatTrabajador;
import com.gestionincidencia.service.impl.CatTrabajadorServiceImpl;


@Controller
@RequestMapping("/catalogo/trabajador")
public class CatTrabajadorController {
	
	@Autowired
	@Qualifier("catTrabajadorServiceImpl")
	private CatTrabajadorServiceImpl serviceImpl;

	private final Log log = LogFactory.getLog(CatLenguajeProgramacion.class);

	@GetMapping("/")
	public String redireccionar() {
		return "redirect:/catalogo/trabajador/listar";
	}
	
	@GetMapping("/listar")
	public String formulario(Model model) {
		model.addAttribute("listCLP", serviceImpl.selectAll());
		return Vistas.LISTAR_CAT_TRABAJADOR;
	}
	
	
	@GetMapping("/addFormulario")
	public String agregarFormulario(@RequestParam(name = "id", required = true) int id,
	Model model) {
		CatTrabajador programacion = new CatTrabajador(0L);
		if (id != 0) {
			programacion = serviceImpl.selectOne((long) id);
		}
		log.info("agregarFormulario: " + programacion.toString());
		model.addAttribute("objectCLP", programacion);
		return Vistas.FORMULARIO_CAT_TRABAJADOR;
	}
	
	
	@PostMapping("/addOrEdit")
	public String agregar(@ModelAttribute(name="objectCLP") CatTrabajador catTrabajador, Model model) {
		
		log.info("object: " + catTrabajador.toString());
		serviceImpl.create(catTrabajador);
		return formulario(model);
	}
	
	
	@GetMapping("/remove")
	public String eliminar(@RequestParam(name = "id", required = true) int id, Model model){
		serviceImpl.delete((long) id);
		return formulario(model);
	}
}
