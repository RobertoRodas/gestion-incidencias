package com.gestionincidencia.controller;

import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.gestionincidencia.entity.CatLenguajeProgramacion;
import com.gestionincidencia.entity.CatProgramacion;
import com.gestionincidencia.entity.CatProyectos;
import com.gestionincidencia.entity.CatTipoProyecto;
import com.gestionincidencia.repository.CatLenguajeProgramacionRepository;
import com.gestionincidencia.repository.CatProgramacionRepository;
import com.gestionincidencia.repository.CatTipoProyectoRepository;
import com.gestionincidencia.service.impl.CatProyectosServiceImpl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/catalogo/proyectos")
public class CatProyectoController {

    @Autowired
    @Qualifier("catProyectosServiceImpl")
    private CatProyectosServiceImpl serviceImpl;

    @Autowired
    @Qualifier("catTipoProyectoRepository")
    private CatTipoProyectoRepository tipoProyectoRepository;

    @Autowired
    @Qualifier("catLenguajeProgramacionRepository")
    private CatLenguajeProgramacionRepository lenguajeProgramacionRepository;

    @Autowired
    @Qualifier("catProgramacionRepository")
    private CatProgramacionRepository programacionRepository;

    private final Log log = LogFactory.getLog(CatProyectoController.class);

    /**
     * @return String
     */
    @GetMapping("/")
    public String redireccionar() {
        return "redirect:/catalogo/proyectos/listar";
    }

    /**
     * @param model
     * @return String
     */
    @GetMapping("/listar")
    public String formulario(Model model) {
        model.addAttribute("listCLP", serviceImpl.selectAll());
        return Vistas.LISTAR_CAT_PROYECTOS;
    }

    /**
     * @param "id"
     * @param id
     * @param model
     * @return String
     */
    @GetMapping("/addFormulario")
    public String agregarFormulario(@RequestParam(name = "id", required = true) int id, Model model) {
        CatProyectos programacion = new CatProyectos(0L);
        if (id != 0) {
            programacion = serviceImpl.selectOne((long) id);
        }

        // log.info("agregarFormulario(): " + programacion.toString());

        
        model.addAttribute("objectCLP", programacion);
        model.addAttribute("catLenguaje", new CatLenguajeProgramacion());
        model.addAttribute("tipoProyecto", new CatTipoProyecto());
        model.addAttribute("catProgramacion", new CatProgramacion());
        model.addAttribute("catProgramacions", programacionRepository.findByEstatusTrue());
        model.addAttribute("catLenguajes", lenguajeProgramacionRepository.findByEstatusTrue());
        model.addAttribute("tipoProyectos", tipoProyectoRepository.findByEstatusTrue());
        // log.info(model.toString());
        return Vistas.FORMULARIO_CAT_PROYECTOS;
    }

    /**
     * 
     * @param catProyectos
     * @param detProyecto
     * @param model
     * @return
     */
    @PostMapping("/addOrEdit")
    public String agregar(@ModelAttribute(name = "objectCLP") CatProyectos catProyectos,
            @ModelAttribute(name = "catProgramacions") List<CatProgramacion> catProgramacions,
            Model model) {
        log.info("agregar(): ");
        //@ModelAttribute(name = "detbasedatos") DetBasedatos basedatos,
        if (catProyectos.getIdCatProyecto() == 0) {
            catProyectos.setEstatus(true);
            catProyectos.setFechaInicio(Calendar.getInstance().getTime());
        }

        
        Set<CatProgramacion> catProgramacions2 = new HashSet<CatProgramacion>();
        catProgramacions2.addAll(catProgramacions);

        catProyectos.setCatProgramacions(catProgramacions2);
        
        
        // log.info("agregar() catProyecto: " + catProyectos.toString());
        // log.info("agregar() detProyecto: " + detProyecto.getCatProgramacions().toString());
        // log.info("agregar() detBasedato: " + basedatos.toString());
        serviceImpl.create(catProyectos);
        return formulario(model);
        // return Vistas.FORMULARIO_CAT_PROYECTOS;
    }

    /**
     * @param "id"
     * @param id
     * @param model
     * @return String
     */
    @GetMapping("/remove")
    public String eliminar(@RequestParam(name = "id", required = true) int id, Model model) {
        serviceImpl.delete((long) id);
        return formulario(model);
    }

}