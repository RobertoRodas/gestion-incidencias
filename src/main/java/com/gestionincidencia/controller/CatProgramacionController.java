package com.gestionincidencia.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gestionincidencia.entity.CatProgramacion;
import com.gestionincidencia.service.impl.CatProgramacionServiceImpl;


@Controller
@RequestMapping("/catalogo/programacion")
public class CatProgramacionController {

	@Autowired
	@Qualifier("catProgramacionServiceImpl")
	private CatProgramacionServiceImpl  serviceImpl;

	private final Log log = LogFactory.getLog(CatProgramacionController.class);

	@GetMapping("/")
	public String redireccionar() {
		return "redirect:/catalogo/programacion/listar";
	}

	@GetMapping("/listar")
	public String formulario(Model model) {
		model.addAttribute("listCLP", serviceImpl.selectAll());
		return Vistas.LISTAR_CAT_PROGRAMACION;
	}

	@GetMapping("/addFormulario")
	public String agregarFormulario(@RequestParam(name = "id", required = true) int id, Model model) {
		CatProgramacion programacion = new CatProgramacion(0L);
		if (id != 0) {
			programacion = serviceImpl.selectOne((long) id);
		}
		log.info("agregarFormulario: " + programacion.toString());
		model.addAttribute("objectCLP", programacion);
		return Vistas.FORMULARIO_CAT_PROGRAMACION;
	}

	@PostMapping("/addOrEdit")
	public String agregar(@ModelAttribute(name = "objectCLP") CatProgramacion catProgramacion, Model model) {
		if (catProgramacion.getIdCatProgramacion() == 0) {
			catProgramacion.setEstatus(true);
		}

		log.info("object: " + catProgramacion.toString());
		serviceImpl.create(catProgramacion);
		return formulario(model);
	}

	@GetMapping("/remove")
	public String eliminar(@RequestParam(name = "id", required = true) int id, Model model) {
		serviceImpl.delete((long) id);
		return formulario(model);
	}
}
