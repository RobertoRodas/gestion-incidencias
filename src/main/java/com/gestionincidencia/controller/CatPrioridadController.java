package com.gestionincidencia.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gestionincidencia.entity.CatPrioridad;
import com.gestionincidencia.service.impl.CatPrioridadServiceImpl;

@Controller
@RequestMapping("/catalogo/prioridad")
public class CatPrioridadController {

	@Autowired
	@Qualifier("catPrioridadServiceImpl")
	private CatPrioridadServiceImpl serviceImpl;
	

	private final Log log = LogFactory.getLog(CatPrioridadController.class);

	@GetMapping("/")
	public String redireccionar() {
		return "redirect:/catalogo/prioridad/listar";
	}
	
	@GetMapping("/listar")
	public String formulario(Model model) {
		model.addAttribute("listCLP", serviceImpl.selectAll());
		return Vistas.LISTAR_CAT_PRIORIDAD;
	}
	
	
	@GetMapping("/addFormulario")
	public String agregarFormulario(@RequestParam(name = "id", required = true) int id,
	Model model) {
		CatPrioridad programacion = new CatPrioridad(0L);
		if (id != 0) {
			programacion = serviceImpl.selectOne((long) id);
		}
		log.info("agregarFormulario: " + programacion.toString());
		model.addAttribute("objectCLP", programacion);
		return Vistas.FORMULARIO_CAT_PRIORIDAD;
	}
	
	
	@PostMapping("/addOrEdit")
	public String agregar(@ModelAttribute(name="objectCLP") CatPrioridad catPrioridad, Model model) {
		if (catPrioridad.getIdCatPrioridad() == 0) {
			catPrioridad.setEstatus(true);			
		}
		
		log.info("object: " + catPrioridad.toString());
		serviceImpl.create(catPrioridad);
		return formulario(model);
	}
	
	
	@GetMapping("/remove")
	public String eliminar(@RequestParam(name = "id", required = true) int id, Model model){
		serviceImpl.delete((long) id);
		return formulario(model);
	}
	
}
