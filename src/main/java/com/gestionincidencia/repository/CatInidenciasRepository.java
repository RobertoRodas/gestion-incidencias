package com.gestionincidencia.repository;

import com.gestionincidencia.entity.CatIncidencia;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("catInidenciasRepository")
public interface CatInidenciasRepository extends JpaRepository<CatIncidencia, Long> {

}