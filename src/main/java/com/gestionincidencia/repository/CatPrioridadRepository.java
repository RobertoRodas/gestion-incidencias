package com.gestionincidencia.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gestionincidencia.entity.CatPrioridad;


@Repository("catPrioridadRepository")
public interface CatPrioridadRepository extends JpaRepository<CatPrioridad, Long> {

}
