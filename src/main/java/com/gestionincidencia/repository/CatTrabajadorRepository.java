package com.gestionincidencia.repository;

import com.gestionincidencia.entity.CatTrabajador;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("catTrabajadorRepository")
public interface CatTrabajadorRepository extends JpaRepository<CatTrabajador, Long> {

}