package com.gestionincidencia.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.gestionincidencia.entity.CatLenguajeProgramacion;

@Repository("catLenguajeProgramacionRepository")
public interface CatLenguajeProgramacionRepository extends JpaRepository<CatLenguajeProgramacion, Long> {

    public List<CatLenguajeProgramacion> findByEstatusTrue();

}
