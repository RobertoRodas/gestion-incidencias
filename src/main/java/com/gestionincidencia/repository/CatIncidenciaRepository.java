package com.gestionincidencia.repository;

import com.gestionincidencia.entity.CatIncidencia;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("catIncidenciaRepository")
public interface CatIncidenciaRepository extends JpaRepository<CatIncidencia, Long> {

}