package com.gestionincidencia.repository;

import com.gestionincidencia.entity.CatProyectos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("catProyectosRepository")
public interface CatProyectosRepository extends JpaRepository<CatProyectos, Long> {

}