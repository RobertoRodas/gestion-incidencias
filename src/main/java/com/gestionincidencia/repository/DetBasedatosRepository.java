package com.gestionincidencia.repository;

import com.gestionincidencia.entity.DetBasedatos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("detBasedatosRepository")
public interface DetBasedatosRepository extends JpaRepository<DetBasedatos, Long > {

    
}