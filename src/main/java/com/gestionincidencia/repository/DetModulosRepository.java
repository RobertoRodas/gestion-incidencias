package com.gestionincidencia.repository;

import com.gestionincidencia.entity.DetModulos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("detModulosRepository")
public interface DetModulosRepository extends JpaRepository<DetModulos, Long> {

}