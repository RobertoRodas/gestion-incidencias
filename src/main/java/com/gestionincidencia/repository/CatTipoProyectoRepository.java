package com.gestionincidencia.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.gestionincidencia.entity.CatTipoProyecto;

@Repository("catTipoProyectoRepository")
public interface CatTipoProyectoRepository extends JpaRepository<CatTipoProyecto, Long> {

    List<CatTipoProyecto> findByEstatusTrue();

}
