package com.gestionincidencia.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.gestionincidencia.entity.CatProgramacion;

@Repository("catProgramacionRepository")
public interface CatProgramacionRepository extends JpaRepository<CatProgramacion, Long> {

    public List<CatProgramacion> findByEstatusTrue();
}
