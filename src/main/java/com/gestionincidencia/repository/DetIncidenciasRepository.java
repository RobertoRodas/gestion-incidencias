package com.gestionincidencia.repository;

import com.gestionincidencia.entity.DetIncidencia;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("detIncidenciasRepository")
public interface DetIncidenciasRepository extends JpaRepository<DetIncidencia, Long> {

}