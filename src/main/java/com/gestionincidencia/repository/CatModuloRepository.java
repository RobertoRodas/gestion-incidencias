package com.gestionincidencia.repository;

import com.gestionincidencia.entity.CatModulos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("catModuloRepository")
public interface CatModuloRepository extends JpaRepository<CatModulos, Long> {

}