package com.gestionincidencia.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class DetBasedatos {

	@Id
	@GeneratedValue
	private Long cns;

	private String atributo;
	private String valor;

	@ManyToOne(cascade = CascadeType.ALL)
	private CatProyectos idCatProyectos;

	public DetBasedatos() {
	}

	public DetBasedatos(Long cns) {
		this.cns = cns;
	}

	public DetBasedatos(Long cns, String atributo, String valor, CatProyectos catProyectos) {
		super();
		this.cns = cns;
		this.atributo = atributo;
		this.valor = valor;
		this.idCatProyectos = catProyectos;
	}

	public Long getCns() {
		return cns;
	}

	public void setCns(Long cns) {
		this.cns = cns;
	}

	public String getAtributo() {
		return atributo;
	}

	public void setAtributo(String atributo) {
		this.atributo = atributo;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	/**
	 * @param idCatProyectos the idCatProyectos to set
	 */
	public void setIdCatProyectos(CatProyectos idCatProyectos) {
		this.idCatProyectos = idCatProyectos;
	}

	/**
	 * @return the idCatProyectos
	 */
	public CatProyectos getIdCatProyectos() {
		return idCatProyectos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cns == null) ? 0 : cns.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DetBasedatos other = (DetBasedatos) obj;
		if (cns == null) {
			if (other.cns != null)
				return false;
		} else if (!cns.equals(other.cns))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DetBasedatos [cns=" + cns + ", atributo=" + atributo + ", valor=" + valor + "]";
	}

}
