package com.gestionincidencia.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class CatIncidencia {

	@Id
	@GeneratedValue
	private Long idCatIncidencia;

	@ManyToOne(cascade = CascadeType.ALL)
	private CatProyectos idCatProyectos;
	@ManyToOne(cascade = CascadeType.ALL)
	private DetModulos idDetModulos;
	@ManyToOne(cascade = CascadeType.ALL)
	private CatPrioridad idCatPrioridad;

	@OneToMany(mappedBy = "idCatIncidencia")
	private Set<DetIncidencia> detIncidencias;

	public CatIncidencia() {
	}

	public CatIncidencia(Long idCatIncidencia) {
		this.idCatIncidencia = idCatIncidencia;
	}

	public CatIncidencia(Long idCatIncidencia, CatProyectos idCatProyectos, DetModulos idDetModulos,
			CatPrioridad idCatPrioridad) {
		super();
		this.idCatIncidencia = idCatIncidencia;
		this.idCatProyectos = idCatProyectos;
		this.idDetModulos = idDetModulos;
		this.idCatPrioridad = idCatPrioridad;
	}

	public Long getIdCatIncidencia() {
		return idCatIncidencia;
	}

	public void setIdCatIncidencia(Long idCatIncidencia) {
		this.idCatIncidencia = idCatIncidencia;
	}

	public CatProyectos getIdCatProyectos() {
		return idCatProyectos;
	}

	public void setIdCatProyectos(CatProyectos idCatProyectos) {
		this.idCatProyectos = idCatProyectos;
	}

	public DetModulos getIdDetModulos() {
		return idDetModulos;
	}

	public void setIdDetModulos(DetModulos idDetModulos) {
		this.idDetModulos = idDetModulos;
	}

	public CatPrioridad getIdCatPrioridad() {
		return idCatPrioridad;
	}

	public void setIdCatPrioridad(CatPrioridad idCatPrioridad) {
		this.idCatPrioridad = idCatPrioridad;
	}

	public Set<DetIncidencia> getDetIncidencias() {
		return detIncidencias;
	}

	public void setDetIncidencias(Set<DetIncidencia> detIncidencias) {
		this.detIncidencias = detIncidencias;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idCatIncidencia == null) ? 0 : idCatIncidencia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CatIncidencia other = (CatIncidencia) obj;
		if (idCatIncidencia == null) {
			if (other.idCatIncidencia != null)
				return false;
		} else if (!idCatIncidencia.equals(other.idCatIncidencia))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CatIncidencia [idCatIncidencia=" + idCatIncidencia + "]";
	}

}
