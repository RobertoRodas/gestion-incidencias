package com.gestionincidencia.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class CatModulos {

	@Id
	@GeneratedValue
	private Long idCatModulos;

	@ManyToOne(cascade = CascadeType.ALL)
	private CatProyectos idCatProyectos;

	private String modulo;
	private String descripcion;

	@OneToMany(mappedBy = "idCatModulos")
	private Set<DetModulos> detModulos;

	public CatModulos() {
	}

	public CatModulos(Long idCatModulos) {
		this.idCatModulos = idCatModulos;
	}

	public CatModulos(Long idCatModulos, CatProyectos idCatProyectos, String modulo, String descripcion) {
		super();
		this.idCatModulos = idCatModulos;
		this.idCatProyectos = idCatProyectos;
		this.modulo = modulo;
		this.descripcion = descripcion;
	}

	public Set<DetModulos> getDetModulos() {
		return detModulos;
	}

	public void setDetModulos(Set<DetModulos> detModulos) {
		this.detModulos = detModulos;
	}

	public Long getIdCatModulos() {
		return idCatModulos;
	}

	public void setIdCatModulos(Long idCatModulos) {
		this.idCatModulos = idCatModulos;
	}

	public CatProyectos getIdCatProyectos() {
		return idCatProyectos;
	}

	public void setIdCatProyectos(CatProyectos idCatProyectos) {
		this.idCatProyectos = idCatProyectos;
	}

	public String getModulo() {
		return modulo;
	}

	public void setModulo(String modulo) {
		this.modulo = modulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idCatModulos == null) ? 0 : idCatModulos.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CatModulos other = (CatModulos) obj;
		if (idCatModulos == null) {
			if (other.idCatModulos != null)
				return false;
		} else if (!idCatModulos.equals(other.idCatModulos))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CatModulos [idCatModulos=" + idCatModulos + ", idCatProyectos=" + idCatProyectos + ", modulo=" + modulo
				+ ", descripcion=" + descripcion + "]";
	}

}
