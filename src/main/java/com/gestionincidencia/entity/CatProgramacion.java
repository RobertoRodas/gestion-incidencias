package com.gestionincidencia.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class CatProgramacion {

	@Id
	@GeneratedValue
	private Long idCatProgramacion;

	private String descripcion;
	private boolean estatus;

	public CatProgramacion() {
		super();
	}

	public CatProgramacion(Long idCatProgramacion) {
		this.idCatProgramacion = idCatProgramacion;
	}

	public CatProgramacion(Long idCatProgramacion, String descripcion, boolean estatus) {
		super();
		this.idCatProgramacion = idCatProgramacion;
		this.descripcion = descripcion;
		this.estatus = estatus;
	}

	public Long getIdCatProgramacion() {
		return idCatProgramacion;
	}

	public void setIdCatProgramacion(Long idCatProgramacion) {
		this.idCatProgramacion = idCatProgramacion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean isEstatus() {
		return estatus;
	}

	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idCatProgramacion == null) ? 0 : idCatProgramacion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CatProgramacion other = (CatProgramacion) obj;
		if (idCatProgramacion == null) {
			if (other.idCatProgramacion != null)
				return false;
		} else if (!idCatProgramacion.equals(other.idCatProgramacion))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CatProgramacion [idCatProgramacion=" + idCatProgramacion + ", descripcion=" + descripcion + ", estatus="
				+ estatus + "]";
	}

}
