package com.gestionincidencia.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class DetModulos {

	@Id
	@GeneratedValue
	private Long cns;

	@ManyToOne(cascade = CascadeType.ALL)
	private CatModulos idCatModulos;

	private String nombreSubModulo;
	private String descripcion;

	@OneToMany(mappedBy = "idDetModulos")
	private Set<CatIncidencia> catIncidencias;

	public DetModulos() {
	}

	public DetModulos(Long cns) {
		this.cns = cns;
	}

	public DetModulos(Long cns, CatModulos idCatModulos, String nombreSubModulo, String descripcion) {
		super();
		this.cns = cns;
		this.idCatModulos = idCatModulos;
		this.nombreSubModulo = nombreSubModulo;
		this.descripcion = descripcion;
	}

	public CatModulos getIdCatModulos() {
		return idCatModulos;
	}

	public void setIdCatModulos(CatModulos idCatModulos) {
		this.idCatModulos = idCatModulos;
	}

	public Set<CatIncidencia> getCatIncidencias() {
		return catIncidencias;
	}

	public void setCatIncidencias(Set<CatIncidencia> catIncidencias) {
		this.catIncidencias = catIncidencias;
	}

	public Long getCns() {
		return cns;
	}

	public void setCns(Long cns) {
		this.cns = cns;
	}

	public CatModulos getCatModulos() {
		return idCatModulos;
	}

	public void setCatModulos(CatModulos catModulos) {
		this.idCatModulos = catModulos;
	}

	public String getNombreSubModulo() {
		return nombreSubModulo;
	}

	public void setNombreSubModulo(String nombreSubModulo) {
		this.nombreSubModulo = nombreSubModulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cns == null) ? 0 : cns.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DetModulos other = (DetModulos) obj;
		if (cns == null) {
			if (other.cns != null)
				return false;
		} else if (!cns.equals(other.cns))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DetModulos [cns=" + cns + ", catModulos=" + idCatModulos + ", nombreSubModulo=" + nombreSubModulo
				+ ", descripcion=" + descripcion + "]";
	}

}
