package com.gestionincidencia.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class DetIncidencia {

	@Id
	@GeneratedValue
	private Long cns;

	@ManyToOne
	private CatIncidencia idCatIncidencia;
	@ManyToOne
	private CatTrabajador idCatTrabajador;

	@Temporal(TemporalType.DATE)
	private Date fechaInicio;
	@Temporal(TemporalType.DATE)
	private Date fechaFinal;

	private String descripcionProblema;
	private String descripcionSolucion;

	public DetIncidencia() {
	}

	public DetIncidencia(Long cns) {
		this.cns = cns;
	}

	public DetIncidencia(Long cns, CatIncidencia idCatIncidencia, CatTrabajador idCatTrabajador, Date fechaInicio,
			Date fechaFinal, String descripcionProblema, String descripcionSolucion) {
		super();
		this.cns = cns;
		this.idCatIncidencia = idCatIncidencia;
		this.idCatTrabajador = idCatTrabajador;
		this.fechaInicio = fechaInicio;
		this.fechaFinal = fechaFinal;
		this.descripcionProblema = descripcionProblema;
		this.descripcionSolucion = descripcionSolucion;
	}

	public Long getCns() {
		return cns;
	}

	public void setCns(Long cns) {
		this.cns = cns;
	}

	public CatIncidencia getIdCatIncidencia() {
		return idCatIncidencia;
	}

	public void setIdCatIncidencia(CatIncidencia idCatIncidencia) {
		this.idCatIncidencia = idCatIncidencia;
	}

	public CatTrabajador getIdCatTrabajador() {
		return idCatTrabajador;
	}

	public void setIdCatTrabajador(CatTrabajador idCatTrabajador) {
		this.idCatTrabajador = idCatTrabajador;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public String getDescripcionProblema() {
		return descripcionProblema;
	}

	public void setDescripcionProblema(String descripcionProblema) {
		this.descripcionProblema = descripcionProblema;
	}

	public String getDescripcionSolucion() {
		return descripcionSolucion;
	}

	public void setDescripcionSolucion(String descripcionSolucion) {
		this.descripcionSolucion = descripcionSolucion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cns == null) ? 0 : cns.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DetIncidencia other = (DetIncidencia) obj;
		if (cns == null) {
			if (other.cns != null)
				return false;
		} else if (!cns.equals(other.cns))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DetIncidencia [cns=" + cns + ", idCatIncidencia=" + idCatIncidencia + ", idCatTrabajador="
				+ idCatTrabajador + ", fechaInicio=" + fechaInicio + ", fechaFinal=" + fechaFinal
				+ ", descripcionProblema=" + descripcionProblema + ", descripcionSolucion=" + descripcionSolucion + "]";
	}

}
