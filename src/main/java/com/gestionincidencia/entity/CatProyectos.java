package com.gestionincidencia.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class CatProyectos {

	@Id
	@GeneratedValue
	private Long idCatProyecto;
	@ManyToOne
	private CatTipoProyecto idCatTipoProyecto;
	private String nombreProyecto;
	@Temporal(TemporalType.DATE)
	private Date fechaInicio;
	private boolean estatus;

	private String responsable;

	@ManyToOne(cascade = CascadeType.ALL)
	private CatLenguajeProgramacion idCatLenPro;

	@ManyToMany
	@JoinTable(name = "cat_proyecto_cat_programacion", joinColumns = @JoinColumn(name = "id_cat_proyecto"), inverseJoinColumns = @JoinColumn(name = "id_cat_programacion"))
	private Set<CatProgramacion> catProgramacions;

	@OneToMany(mappedBy = "idCatProyectos")
	private Set<DetBasedatos> detBasedatos;

	@OneToMany(mappedBy = "idCatProyectos")
	private Set<CatModulos> catModulos;

	@OneToMany(mappedBy = "idCatProyectos")
	private Set<CatIncidencia> catIncidencias;

	/**
	 * @return
	 */
	public CatProyectos() {

	}

	/**
	 * @param idCatProyecto
	 * @return
	 */
	public CatProyectos(Long idCatProyecto) {
		this.idCatProyecto = idCatProyecto;
	}

	/**
	 * @param idCatProyecto
	 * @param idCatTipoProyecto
	 * @param nombreProyecto
	 * @param fechaInicio
	 * @param estatus
	 * @return
	 */
	public CatProyectos(Long idCatProyecto, CatTipoProyecto idCatTipoProyecto, String nombreProyecto, Date fechaInicio,
			boolean estatus) {
		super();
		this.idCatProyecto = idCatProyecto;
		this.idCatTipoProyecto = idCatTipoProyecto;
		this.nombreProyecto = nombreProyecto;
		this.fechaInicio = fechaInicio;
		this.estatus = estatus;
	}

	/**
	 * @return the catProgramacions
	 */
	public Set<CatProgramacion> getCatProgramacions() {
		return catProgramacions;
	}

	/**
	 * @return the idCatLenPro
	 */
	public CatLenguajeProgramacion getIdCatLenPro() {
		return idCatLenPro;
	}

	/**
	 * @return the responsable
	 */
	public String getResponsable() {
		return responsable;
	}

	/**
	 * @param detBasedatos the detBasedatos to set
	 */
	public void setDetBasedatos(Set<DetBasedatos> detBasedatos) {
		this.detBasedatos = detBasedatos;
	}

	/**
	 * @return the detBasedatos
	 */
	public Set<DetBasedatos> getDetBasedatos() {
		return detBasedatos;
	}

	/**
	 * @param catProgramacions the catProgramacions to set
	 */
	public void setCatProgramacions(Set<CatProgramacion> catProgramacions) {
		this.catProgramacions = catProgramacions;
	}

	/**
	 * @param idCatLenPro the idCatLenPro to set
	 */
	public void setIdCatLenPro(CatLenguajeProgramacion idCatLenPro) {
		this.idCatLenPro = idCatLenPro;
	}

	/**
	 * @param responsable the responsable to set
	 */
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	/**
	 * @return Set<CatIncidencia>
	 */
	public Set<CatIncidencia> getCatIncidencias() {
		return catIncidencias;
	}

	/**
	 * @param catIncidencias
	 */
	public void setCatIncidencias(Set<CatIncidencia> catIncidencias) {
		this.catIncidencias = catIncidencias;
	}

	/**
	 * @return Set<CatModulos>
	 */
	public Set<CatModulos> getCatModulos() {
		return catModulos;
	}

	/**
	 * @param catModulos
	 */
	public void setCatModulos(Set<CatModulos> catModulos) {
		this.catModulos = catModulos;
	}

	/**
	 * @return Long
	 */
	public Long getIdCatProyecto() {
		return idCatProyecto;
	}

	/**
	 * @param idCatProyecto
	 */
	public void setIdCatProyecto(Long idCatProyecto) {
		this.idCatProyecto = idCatProyecto;
	}

	/**
	 * @return CatTipoProyecto
	 */
	public CatTipoProyecto getIdCatTipoProyecto() {
		return idCatTipoProyecto;
	}

	/**
	 * @param idCatTipoProyecto
	 */
	public void setIdCatTipoProyecto(CatTipoProyecto idCatTipoProyecto) {
		this.idCatTipoProyecto = idCatTipoProyecto;
	}

	/**
	 * @return String
	 */
	public String getNombreProyecto() {
		return nombreProyecto;
	}

	/**
	 * @param nombreProyecto
	 */
	public void setNombreProyecto(String nombreProyecto) {
		this.nombreProyecto = nombreProyecto;
	}

	/**
	 * @return Date
	 */
	public Date getFechaInicio() {
		return fechaInicio;
	}

	/**
	 * @param fechaInicio
	 */
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	/**
	 * @return boolean
	 */
	public boolean isEstatus() {
		return estatus;
	}

	/**
	 * @param estatus
	 */
	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return int
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idCatProyecto == null) ? 0 : idCatProyecto.hashCode());
		return result;
	}

	/**
	 * @param obj
	 * @return boolean
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CatProyectos other = (CatProyectos) obj;
		if (idCatProyecto == null) {
			if (other.idCatProyecto != null)
				return false;
		} else if (!idCatProyecto.equals(other.idCatProyecto))
			return false;
		return true;
	}

	/**
	 * @return String
	 */
	@Override
	public String toString() {
		return "CatProyectos [idCatProyecto=" + idCatProyecto + ", idCatTipoProyecto=" + idCatTipoProyecto
				+ ", nombreProyecto=" + nombreProyecto + ", fechaInicio=" + fechaInicio + ", responsable=" + responsable
				+ ", estatus=" + estatus + "]";
	}

}
