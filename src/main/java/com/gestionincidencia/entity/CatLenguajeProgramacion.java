package com.gestionincidencia.entity;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class CatLenguajeProgramacion {

	@Id
	@GeneratedValue
	private Long idCatLenPro;

	private String descripcion;
	private boolean estatus;

	@OneToMany(mappedBy = "idCatLenPro")
	private Set<CatProyectos> catProyectos;

	public CatLenguajeProgramacion() {
	}

	public CatLenguajeProgramacion(Long idCatLenPro) {
		super();
		this.idCatLenPro = idCatLenPro;
	}

	public CatLenguajeProgramacion(Long idCatLenPro, String descripcion, boolean estatus) {
		super();
		this.idCatLenPro = idCatLenPro;
		this.descripcion = descripcion;
		this.estatus = estatus;
	}

	/**
	 * @return the catProyectos
	 */
	public Set<CatProyectos> getCatProyectos() {
		return catProyectos;
	}

	/**
	 * @param catProyectos the catProyectos to set
	 */
	public void setCatProyectos(Set<CatProyectos> catProyectos) {
		this.catProyectos = catProyectos;
	}

	public Long getIdCatLenPro() {
		return idCatLenPro;
	}

	public void setIdCatLenPro(Long idCatLenPro) {
		this.idCatLenPro = idCatLenPro;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean isEstatus() {
		return estatus;
	}

	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idCatLenPro == null) ? 0 : idCatLenPro.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CatLenguajeProgramacion other = (CatLenguajeProgramacion) obj;
		if (idCatLenPro == null) {
			if (other.idCatLenPro != null)
				return false;
		} else if (!idCatLenPro.equals(other.idCatLenPro))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CatLenguajeProgramacion [idCatLenPro=" + idCatLenPro + ", descripcion=" + descripcion + ", estatus="
				+ estatus + "]";
	}

}
