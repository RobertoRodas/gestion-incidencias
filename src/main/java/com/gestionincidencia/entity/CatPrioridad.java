package com.gestionincidencia.entity;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class CatPrioridad {

	@Id
	@GeneratedValue
	private Long idCatPrioridad;

	private String descripcion;
	private boolean estatus;

	@OneToMany(mappedBy = "idCatPrioridad")
	private Set<CatIncidencia> catIncidencias;

	public CatPrioridad() {
	}

	public CatPrioridad(Long idCatPrioridad) {
		this.idCatPrioridad = idCatPrioridad;
	}

	public CatPrioridad(Long idCatPrioridad, String descripcion, boolean estatus) {
		super();
		this.idCatPrioridad = idCatPrioridad;
		this.descripcion = descripcion;
		this.estatus = estatus;
	}

	public Set<CatIncidencia> getCatIncidencias() {
		return catIncidencias;
	}

	public void setCatIncidencias(Set<CatIncidencia> catIncidencias) {
		this.catIncidencias = catIncidencias;
	}

	public Long getIdCatPrioridad() {
		return idCatPrioridad;
	}

	public void setIdCatPrioridad(Long idCatPrioridad) {
		this.idCatPrioridad = idCatPrioridad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean isEstatus() {
		return estatus;
	}

	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idCatPrioridad == null) ? 0 : idCatPrioridad.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CatPrioridad other = (CatPrioridad) obj;
		if (idCatPrioridad == null) {
			if (other.idCatPrioridad != null)
				return false;
		} else if (!idCatPrioridad.equals(other.idCatPrioridad))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CatPrioridad [idCatPrioridad=" + idCatPrioridad + ", descripcion=" + descripcion + ", estatus="
				+ estatus + "]";
	}

}
