package com.gestionincidencia.entity;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class CatTipoProyecto {

	@Id
	@GeneratedValue
	private Long idCatTipoProyecto;

	private String descripcion;
	private boolean estatus;

	@OneToMany(mappedBy = "idCatTipoProyecto")
	private Set<CatProyectos> catProyectos;

	public CatTipoProyecto() {
	}

	public CatTipoProyecto(Long idCatTipoProyecto) {
		this.idCatTipoProyecto = idCatTipoProyecto;
	}

	public CatTipoProyecto(Long idCatTipoProyecto, String descripcion, boolean estatus) {
		super();
		this.idCatTipoProyecto = idCatTipoProyecto;
		this.descripcion = descripcion;
		this.estatus = estatus;
	}

	public Set<CatProyectos> getCatProyectos() {
		return catProyectos;
	}

	public void setCatProyectos(Set<CatProyectos> catProyectos) {
		this.catProyectos = catProyectos;
	}

	public Long getIdCatTipoProyecto() {
		return idCatTipoProyecto;
	}

	public void setIdCatTipoProyecto(Long idCatTipoProyecto) {
		this.idCatTipoProyecto = idCatTipoProyecto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean isEstatus() {
		return estatus;
	}

	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idCatTipoProyecto == null) ? 0 : idCatTipoProyecto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CatTipoProyecto other = (CatTipoProyecto) obj;
		if (idCatTipoProyecto == null) {
			if (other.idCatTipoProyecto != null)
				return false;
		} else if (!idCatTipoProyecto.equals(other.idCatTipoProyecto))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CatTipoProyecto [idCatTipoProyecto=" + idCatTipoProyecto + ", descripcion=" + descripcion + ", estatus="
				+ estatus + "]";
	}
}
