package com.gestionincidencia.entity;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class CatTrabajador {

	@Id
	@GeneratedValue
	private Long idCatTrabajador;

	private String nombre;
	private String apePat;
	private String apeMat;
	private String grado;

	@OneToMany(mappedBy = "idCatTrabajador")
	private Set<DetIncidencia> detIncidencias;

	public CatTrabajador() {
	}

	public CatTrabajador(Long idCatTrabajador) {
		this.idCatTrabajador = idCatTrabajador;
	}

	public CatTrabajador(Long idCatTrabajador, String nombre, String apePat, String apeMat, String grado) {
		super();
		this.idCatTrabajador = idCatTrabajador;
		this.nombre = nombre;
		this.apePat = apePat;
		this.apeMat = apeMat;
		this.grado = grado;
	}

	public Set<DetIncidencia> getDetIncidencias() {
		return detIncidencias;
	}

	public void setDetIncidencias(Set<DetIncidencia> detIncidencias) {
		this.detIncidencias = detIncidencias;
	}

	public Long getIdCatTrabajador() {
		return idCatTrabajador;
	}

	public void setIdCatTrabajador(Long idCatTrabajador) {
		this.idCatTrabajador = idCatTrabajador;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApePat() {
		return apePat;
	}

	public void setApePat(String apePat) {
		this.apePat = apePat;
	}

	public String getApeMat() {
		return apeMat;
	}

	public void setApeMat(String apeMat) {
		this.apeMat = apeMat;
	}

	public String getGrado() {
		return grado;
	}

	public void setGrado(String grado) {
		this.grado = grado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idCatTrabajador == null) ? 0 : idCatTrabajador.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CatTrabajador other = (CatTrabajador) obj;
		if (idCatTrabajador == null) {
			if (other.idCatTrabajador != null)
				return false;
		} else if (!idCatTrabajador.equals(other.idCatTrabajador))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CatTrabajador [idCatTrabajador=" + idCatTrabajador + ", nombre=" + nombre + ", apePat=" + apePat
				+ ", apeMat=" + apeMat + ", grado=" + grado + "]";
	}

}
