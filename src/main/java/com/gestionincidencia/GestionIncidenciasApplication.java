package com.gestionincidencia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionIncidenciasApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionIncidenciasApplication.class, args);
	}

}
